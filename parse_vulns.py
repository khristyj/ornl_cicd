import os
import json
from xml.etree import ElementTree as ET

supported_reports = ["gl-secret-detection-report.json", "gl-sast-report.json"]

def load_report(file):
    with open(file) as json_file:
        vulns = [v for v in json.load(json_file)['vulnerabilities']]
    # Process tables to string
    for vuln in vulns:
        for key in vuln.keys():
            if key == 'identifiers':
                new_val = [f"name: {d['name']}\ntype: {d['type']}\nurl: {d.get('url', 'none')}\n" for d in vuln[key]]
                vuln[key] = '\n'.join(new_val)
            elif key == 'location':
                vuln[key] = f"file: {vuln[key]['file']}\nstart_line: {vuln[key]['start_line']}"    
    return vulns

def dicts_to_table(list_of_dicts, headers = set()):
    '''
    Takes a list of dictionaries and returns a list of list
    Optionally takes a list of headers and outputs sublists
    in a specified order. Dicitionaries without specified keys 
    will return an empty string
    '''
    if not headers:
        for d in list_of_dicts: 
            headers = headers.union(d.keys())
    res = [list(headers)]
    for d in list_of_dicts:
      res.append([d.get(key, '') for key in headers]) 
    return res
     
def html_table(table, table_name = ''):
    table_html = ET.Element('table', attrib={'class': 'table'})
    table_headers = table[0]
    for i, row in enumerate(table):
        row_html = ET.Element('tr', attrib={'class': 'row'})
        for n, data in enumerate(row):
            if i == 0:
                cell = ET.Element('th', attrib={'class': 'header'})
            else:
                cell = ET.Element('td', attrib={'class': table_headers[n]})
            row_html.append(cell)
            cell.text = data
        table_html.append(row_html)
    return table_html

def write_html(html, fname = 'output.html', css = ''):
    with open(fname, 'w') as f:
        base = ET.Element('html')
        head = ET.Element('head')
        body = ET.Element('body')
        base.append(head)
        # insert CSS
        if css:
            style = ET.Element('style')
            head.append(style)
            style.text = css
        head.append(body)
        body.append(html)
        ET.ElementTree(base).write(fname, encoding='unicode', method='html')

css_def = '''
table {
  width: 100%;
}
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  padding: 8px;
  vertical-align: top;
  font-size: 14px; 
}
.description, .identifiers, .location {
  white-space: pre-wrap;
}
.name {
  width: 15%;
}
th {
  background-color: #333;
  color: white;
  font-size: 20px;
}
tr:nth-child(odd) {
  background-color: #eee;
}
tr:nth-child(even) {
  background-color: #fff;
}
'''

vulns = []
current_dir = os.listdir()
for file in supported_reports:
    if file in current_dir:
        vulns.extend(load_report(file))
headers = ['name','category','severity', 'identifiers', 'description', 'location']
table = dicts_to_table(vulns, headers)
res = html_table(table)
write_html(res, fname = "security_report.html", css = css_def)
